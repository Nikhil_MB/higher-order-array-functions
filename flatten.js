// Flattens a nested array (the nesting can be to any depth).
// Hint: You can solve this using recursion.
// Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];

var array = [];
function flatten(elements) {
  for (let index = 0; index < elements.length; index++) {
    Array.isArray(elements[index])
      ? flatten(elements[index])
      : array.push(elements[index]);
  }
  return array;
}

module.exports = flatten;
