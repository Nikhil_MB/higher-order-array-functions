// How reduce works: A reduce function combines all elements into a single value going from left to right.
// Elements will be passed one by one into `cb` along with the `startingValue`.
// `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
// `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.

function reduce(elements, cb, startingValue) {
  let accumulator,
    currentValue,
    index,
    array = elements;

  if (startingValue) {
    accumulator = startingValue;
    index = 0;
  } else {
    accumulator = elements[0];
    index = 1;
  }

  for (index; index < elements.length; index++) {
    currentValue = elements[index];
    accumulator = cb(accumulator, currentValue, index, array);
  }
  return accumulator;
}

module.exports = reduce;
