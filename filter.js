// Do NOT use .filter, to complete this function.
// Similar to `find` but you will return an array of all elements that passed the truth test
// Return an empty array if no elements pass the truth test

function filter(elements, cb) {
  let filterArray = [];
  for (let index = 0; index < elements.length; index++) {
    if (cb(elements[index], index, elements)) {
      filterArray.push(elements[index]);
    }
  }
  return filterArray;
}

module.exports = filter;
