const reduce = require("../reduce");

const items = [1, 2, 3, 4, 5, 5];

const result = reduce(
  items,
  (accumulator, currentValue) => accumulator + currentValue,
  1
);

console.log(result);
