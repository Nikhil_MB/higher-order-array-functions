// How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
// Produces a new array of values by mapping each value in list through a transformation function (iteratee).
// Return the new array.

const each = require("./each");

function map(elements, cb) {
  let array = [];

  each(elements, (element, index) => array.push(cb(element, index, elements)));

  return array;
}

module.exports = map;
